package com.conygre.training.tradesimulator.sim;
import com.conygre.training.tradesimulator.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
//import java.io.IOException; 
//import java.util.logging.Level; 
//import java.util.logging.Logger; 
//import java.util.logging.*; 

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.dao.UserMongoDao;
import com.conygre.training.tradesimulator.model.Trade;
import com.conygre.training.tradesimulator.model.TradeState;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TradeSim {
	//private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	 private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    @Autowired
    private UserMongoDao userDao;

    public List<User> getUsers() {
		return userDao.findAll();
	}
    
    public void processingTrades()
    {
    	List<User> users = getUsers();
    	for(User u: users)
    	{
    		//User user = userDao.findBy_id(new ObjectId(u.get_id())); 
    		List<Trade> trades = new ArrayList<>();
    		if(u.getTrades()!=null)
    		{
    			trades.addAll(u.getTrades());
    			List<Trade> trades1 = findTradesForProcessing(trades);
    			u.setTrades(trades1);
    			userDao.save(u);
    		}
    	}
    }
    
    public void fillingTrades()
    {
    	List<User> users = getUsers();
    	for(User u: users)
    	{
    		//User user = userDao.findBy_id(new ObjectId(u.get_id())); 
    		List<Trade> trades = new ArrayList<>();
    		if(u.getTrades()!=null)
    		{
    			trades.addAll(u.getTrades());
        		List<Trade> trades1 = findTradesForFilling(trades);
        		u.setTrades(trades1);
        		userDao.save(u);
    		}
    	}
    }
    @Transactional
    public List<Trade> findTradesForProcessing(List<Trade> trades){

        for(Trade thisTrade: trades) {
        	if(thisTrade.getState().equals(TradeState.State.CREATED))
        		thisTrade.setState(TradeState.State.PROCESSING);
        }

        return trades;
    }

    @Transactional
    public List<Trade> findTradesForFilling(List<Trade> trades){

        for(Trade thisTrade: trades) {
        	if(thisTrade.getState().equals(TradeState.State.PROCESSING))
        	{
        		if((int) (Math.random()*10) > 8) {
                    thisTrade.setState(TradeState.State.REJECTED);
                }
                else {
                    thisTrade.setState(TradeState.State.FILLED);
                }
        	}
        }
        return trades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    public void runSim() {
    	//LOGGER.log(Level.INFO, "Main loop running!");
        LOG.info("Main loop running!");

        processingTrades();
        fillingTrades();
        //int tradesForFilling = findTradesForFilling().size();
        //LOG.info("Found "+ tradesForFilling + " trades to be filled/rejected");
        //LOGGER.log(Level.INFO, "Found "+ tradesForFilling + " trades to be filled/rejected");

        //int tradesForProcessing = findTradesForProcessing().size();
        //LOG.info("Found " + tradesForProcessing + " trades to be processed");
        //LOGGER.log(Level.INFO, "Found " + tradesForProcessing + " trades to be processed");

    }
}
